// import assert from 'assert'
// import ganache from 'ganache-cli'
// import Web3 from 'web3'
const assert = require("assert")
// const ganache = require('ganache-cli')
const Web3 = require('web3')
// const web3 = new Web3(ganache.provider());

const Factory = artifacts.require("FundingProjectFactory");
const Project = artifacts.require("FundingProject");

let factory;
let project;
let deployedProjects;

contract("factory test", async accounts => {

    
    describe("test", () => {
        
        beforeEach(async () => {
            factory = await Factory.new();
            await factory.createProject("100")
            deployedProjects = await factory.getDeployedProjects();
            project = await Project.at(deployedProjects[0]);
        })

        it("funding project", async () => {

            assert.ok(factory.address)
            assert.ok(project)
        })
    
        it('marks caller as the funding project manager', async () => {

            const manager = await project.manager.call()
            assert.equal(accounts[0], manager);
        })

        it('allows people to contribute money and marks them as approvers', async () => {
            await project.contribute.sendTransaction({
                value: '200',
                from: accounts[1]
            })
            const isContributor = await project.approvers.call(accounts[1]);
            assert(isContributor);
        });

        it('requires a minium contribution', async () => {
            try {
                await project.contribute.sendTransaction({
                    value: '5',
                    from: accounts[1]
                });
                assert(false);
            } catch (err) {
                assert(err);
            }
        });

        it('allows a manager to make a payment', async () => {
            await project.createRequest("Buy Car", '100', accounts[1], {from: accounts[0],
                gas: '3000000'})
            const request = await project.requests.call(0);

            assert.equal("Buy Car", request.description);
        });

        it('process requests', async () => {
            await project.contribute({
                from: accounts[0],
                value: web3.utils.toWei('10', 'ether')
            })
    
            await project
                .createRequest('A', web3.utils.toWei('5', 'ether'), accounts[1], {
                    from: accounts[0],
                    gas: '3000000'
                })
            
            await project.approveRequest(0, {
                from: accounts[0],
                gas: '3000000'
            })

            await project.finalizeRequest(0, {
                from: accounts[0],
                gas: '5000000'
            })

            let balance = await web3.eth.getBalance(accounts[1]);
            balance = web3.utils.fromWei(balance, 'ether');
            balance = parseFloat(balance);
            assert(balance > 104);
        })
    })
})