import { useWeb3React } from '@web3-react/core';
import React, { useEffect, useState } from 'react'
import { Button, Form, Spinner } from 'react-bootstrap'
import { useForm } from 'react-hook-form';
import { useParams } from 'react-router';
import { getContract } from 'utils';
import FundingProjectContract from "../contracts/FundingProject.json";

export default function CreateRequest(props) {
    const labelStyle = {
        textAlign: `left`
    }
    const { register, handleSubmit } = useForm();
    const context = useWeb3React()
    const { active, account, library } = context
    const [loading, setLoading] = useState(false)
    const { address } = useParams();
    const [contract, setContract] = useState(null)
    useEffect(() => {
        if (active) {
            setContract(getContract(address, FundingProjectContract.abi, library, account));
        }
    }, [address, library, account, active])
    const onSubmit = async (data) => {
        let isMounted = true
        try {
            setLoading(true)
            if (contract) {
                const result = await contract.createRequest(data.description, data.value, data.recipient)
                const transactionDetail = await result.wait()
                if (!isMounted) return
                if (transactionDetail.status) {
                    props.setIsCreateRequest(false)
                    alert("Success!!~~")
                }
            }
        } catch (e) {
            console.error(e)
        }
        setLoading(false)

        return (() => {
            isMounted = false
        })
    };
    return (
        <div>
            <Form onSubmit={handleSubmit(onSubmit)}>
                <Form.Group style={labelStyle} controlId="formDescription">
                    <Form.Label>Description</Form.Label>
                    <Form.Control type="text" placeholder="Enter Description" {...register("description")} />
                </Form.Group>
                <Form.Group style={labelStyle} controlId="formValue">
                    <Form.Label>Value</Form.Label>
                    <Form.Control type="number" placeholder="Enter Value" {...register("value")} />
                </Form.Group>
                <Form.Group style={labelStyle} controlId="formRecipient">
                    <Form.Label>Recipient</Form.Label>
                    <Form.Control type="text" placeholder="Enter Recipient" {...register("recipient")} />
                </Form.Group>
                {loading ?
                    <Spinner animation="border" variant="primary" /> :
                    <Button variant="primary" type="submit">
                        Create Request
                </Button>
                }
            </Form>
        </div>
    )
}
