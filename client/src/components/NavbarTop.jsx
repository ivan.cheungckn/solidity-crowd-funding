import { useWeb3React } from '@web3-react/core'
import { injected } from 'connectors'
import React from 'react'
import { Button } from 'react-bootstrap'
import Navbar from 'react-bootstrap/Navbar'
import { Link } from 'react-router-dom'

const LogoHeaderStyle = { fontWeight: 600 }
const buttonContainerStyle = {
    justifyContent: "end",
    flexDirection: "column",
    alignItems: "flex-end"
}
const brandStyle = {
    alignItems: "baseline"
}
export default function NavbarTop(props) {
    const context = useWeb3React()
    const { activate, account } = context
    return (
        <Navbar style={brandStyle}>
            <Navbar.Brand><Link to="/"><Button variant="info" style={LogoHeaderStyle}>Crowd Funding 💸</Button></Link></Navbar.Brand>
            <Navbar.Collapse style={buttonContainerStyle}>
                {account && account !== "0" ? <Button variant="info" className="ml-2">Connected</Button> : <Button variant="info" className="ml-2" onClick={() => activate(injected)}>Connect to wallet</Button>}
                <Link to="/createFundingProject" className="ml-2 mt-2"><Button variant="warning">Funding Project ➕ </Button></Link>
            </Navbar.Collapse>
        </Navbar>
    )
}
