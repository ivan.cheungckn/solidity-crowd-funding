import React from 'react'
import { Table } from 'semantic-ui-react'
import RequestRow from './RequestRow'


export default function RequestTable(props) {
    const renderRow = () => {
        return props.requests.map((request, i) => {
            return <RequestRow 
            isApprovers={props.isApprovers} 
            isUpdate={props.isUpdate} 
            setIsUpdate={props.setIsUpdate} 
            key={i} 
            contract={props.contract} 
            request={request} 
            id={i} 
            approversCount={props.approversCount}
            manager={props.manager}
            />
        })
    }
    return (
        <div style={{ overflow: "auto" }}>
            <Table>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>ID</Table.HeaderCell>
                        <Table.HeaderCell>Description</Table.HeaderCell>
                        <Table.HeaderCell>Amount</Table.HeaderCell>
                        <Table.HeaderCell>Recipient</Table.HeaderCell>
                        <Table.HeaderCell>Approval Count</Table.HeaderCell>
                        <Table.HeaderCell>Approve</Table.HeaderCell>
                        <Table.HeaderCell>Finalize</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {renderRow()}
                </Table.Body>
            </Table>
        </div>
    )
}
