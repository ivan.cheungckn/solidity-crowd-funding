import React, { useEffect, useState } from 'react'
import { Table } from 'semantic-ui-react'
import { Button, Spinner } from 'react-bootstrap';
import { useWeb3React } from '@web3-react/core';
import { ethers } from 'ethers'
export default function RequestRow(props) {
    const context = useWeb3React()
    const { account } = context
    const [isApproved, setIsApproved] = useState(false);
    const [isApproving, setIsApproving] = useState(false);
    const [isFinalizing, setIsFinalizing] = useState(false);

    useEffect(() => {
        async function isApproved() {
            if (props.contract) {
                const isApproved = await props.contract.checkApproved(props.id)
                setIsApproved(isApproved);
            }
        }
        isApproved()
    }, [props.contract, account, props.id])
    const onApprove = async () => {
        let mount = true
        if (props.contract) {
            try {
                setIsApproving(true)
                const result = await props.contract.approveRequest(props.id)
                const transactionDetails = await result.wait()
                if (!mount) return
                setIsApproving(false)
                if (transactionDetails.status) {
                    props.setIsUpdate(!props.isUpdate)
                    alert("Success !!")
                    setIsApproved(true)
                }
            } catch (e) {
                console.error(e)
                alert('fail to approve request!!')
            }
        }
        return (() => {
            mount = false
        })
    }
    const onFinalize = async () => {
        let mount = true
        if (props.contract) {
            try {
                setIsFinalizing(true)
                const result = await props.contract.finalizeRequest(props.id)
                const transactionDetails = await result.wait()
                if (!mount) return
                setIsFinalizing(false)
                if (transactionDetails.status) {
                    props.setIsUpdate(!props.isUpdate)
                    alert("Success !!")
                }
            } catch (e) {
                console.error(e)
                alert('fail to finalize request!!')
            }
        }
        return (() => {
            mount = false
        })
    }
    return (
        <Table.Row>
            <Table.Cell>{props.id}</Table.Cell>
            <Table.Cell>{props.request.description}</Table.Cell>
            <Table.Cell>{ethers.utils.formatEther(props.request.value)}</Table.Cell>
            <Table.Cell>{props.request.recipient}</Table.Cell>
            <Table.Cell>{`${props.request.approvalCount} / ${props.approversCount}`}</Table.Cell>
            <Table.Cell>{props.request.complete ?
                "" : isApproved ?
                    <Button variant="secondary" >Approved</Button> : isApproving ?
                        <Spinner animation="border" variant="primary" /> : props.isApprovers ?
                            <Button variant="success" onClick={onApprove}>Approve</Button> : ""
            }
            </Table.Cell>
            <Table.Cell>{props.request.complete ?
                <Button variant="secondary" >Completed</Button> : props.manager === account ?
                    isFinalizing ? <Spinner animation="border" variant="primary" /> : <Button variant="primary" onClick={onFinalize}>Finalize</Button>
                    : ""}</Table.Cell>
        </Table.Row>
    )
}
