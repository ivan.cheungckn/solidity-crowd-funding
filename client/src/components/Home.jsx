import BlockchainContext from '../BlockchainContext.js';
import React, { useContext, useEffect, useState } from 'react';
import FundingProjectContract from "../contracts/FundingProject.json";

import { Card, CardDeck, Row, Spinner } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { getContract } from 'utils/index.js';
import { useWeb3React } from '@web3-react/core';


export default function Home(props) {
    const [projects, setProjects] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const context = useWeb3React()
    const { library, account } = context
    const blockchainContext = useContext(BlockchainContext);
    const { factoryContract } = blockchainContext;
    const centerStyle= {
        display: "flex",
        justifyContent: "center",
        margin: "50px 5px"
    }
    
    useEffect(() => {
        let fetching = true;
        async function getDeployedProjects() {
            if (factoryContract) {
                setIsLoading(true)
                try{
                    const deployedProjects = await factoryContract.getDeployedProjects();
                    const projectList = []
                    for (const address of deployedProjects) {
                        const project = getContract(address, FundingProjectContract.abi, library, account );
                        const projectTitle = await project.projectTitle();
                        const projectDescription = await project.projectDescription();
                        const minimumContribution = parseInt(await project.minimumContribution());

                        projectList.push({
                            address,
                            projectTitle,
                            projectDescription,
                            minimumContribution
                        })
                    }
                    if (!fetching) return;
                    setProjects(projectList)
                } catch (e) {
                    console.error(e)
                }
                setIsLoading(false)
            }
        }
        getDeployedProjects();
        return (()=>{
            fetching = false
        })
    }, [factoryContract, account, library])
    return (
        <div>
            <Row style={centerStyle}>
                <div>
                    <h2>
                        A Platform for People to Raise Funds using Ethereum. 💰 💰
                    </h2>

                </div>
            </Row>
            {isLoading ? <Spinner animation="border" /> 
            :
            <CardDeck>
                {projects.map((project, i) => {
                    return (<Card key={i}>
                        <Card.Body>
                            <Card.Title>{project.projectTitle}</Card.Title>
                            <Card.Text>
                                {project.projectDescription}
                            </Card.Text>
                            <Card.Text>
                                {project.minimumContribution}
                            </Card.Text>
                            <Card.Text>
                                {project.address}
                            </Card.Text>
                        </Card.Body>
                        <Card.Footer>
                            <Link to={`/fundingProject/${project.address}`}>View Project</Link>
                        </Card.Footer>
                    </Card>)
                })}
            </CardDeck>}
        </div>
    )
}
