import { useWeb3React } from '@web3-react/core';
import React, { useEffect, useState } from 'react'
import { Button, Row } from 'react-bootstrap';
import { useParams } from 'react-router'
import { Link } from 'react-router-dom';
import { getContract } from 'utils';
import FundingProjectContract from "../contracts/FundingProject.json";
import RequestTable from './RequestTable';

export default function Requests() {
    const { address } = useParams();
    const context = useWeb3React()
    const { active, account, library } = context
    const [contract, setContract] = useState();
    const [requests, setRequests] = useState([]);
    const [approversCount, setApproversCount] = useState([]);
    const [isApprovers, setIsApprovers] = useState([]);
    const [manager, setManager] = useState([]);

    const [isUpdate, setIsUpdate] = useState(false)
    useEffect(() => {
        let isMounted = true;
        async function getRequests() {
            if (active && isMounted) {
                const requestArray = []
                const project = getContract(address, FundingProjectContract.abi, library, account)
                const [approversCount, manager] = await Promise.all([project.approversCount(), project.manager()])

                if (project) {
                    const requestNum = await project.requestNum();
                    if (requestNum > 0) {
                        for (let i = 0; i < requestNum; i++) {
                            requestArray.push(await project.requests(i))
                        }
                        setRequests(requestArray)
                    }
                    if (account && account !== "0") {
                        const isApprovers = await project.approvers(account)
                        setIsApprovers(isApprovers)
                    }
                    setContract(project)
                }
                if (!isMounted) return
                setApproversCount(approversCount)
                setManager(manager)
            }
        }

        getRequests();

    }, [active, address, isUpdate, account, library])
    return (
        <div>
            <Row className="mb-3"><Link to={`/fundingProject/${address}`} ><Button variant="secondary"> Back</Button></Link></Row>
            <RequestTable
             manager={manager} 
             isApprovers={isApprovers} 
             isUpdate={isUpdate} 
             setIsUpdate={setIsUpdate} 
             contract={contract} 
             requests={requests} 
             approversCount={approversCount}/>
        </div>
    )
}
