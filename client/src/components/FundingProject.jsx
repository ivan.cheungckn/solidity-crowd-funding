import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router'
import FundingProjectContract from "../contracts/FundingProject.json";
import { Card } from 'semantic-ui-react';
import ContributionForm from './ContributionForm';
import { Button, Col, Row } from 'react-bootstrap'
import CreateRequest from './CreateRequest';
import { Link } from 'react-router-dom';
import { useWeb3React } from '@web3-react/core';
import { getContract } from 'utils';
import {ethers} from 'ethers'
export default function FundingProject() {
    const { address } = useParams();
    const context = useWeb3React()
    const { active ,account, library } = context
    const [projectData, setProjectData] = useState({})

    const [project, setProject] = useState(null);
    const [isContribute, setIsContribute] = useState(false);
    const [isCreateRequest, setIsCreateRequest] = useState(false);
    const buttonStyle = {
        marginTop: `3%`,
        marginRight: `10px`
    }
    useEffect(() => {
        async function getProject() {
            if (active) {
                const project = getContract(address, FundingProjectContract.abi, library, account);
                setProject(project)
                const [projectTitle, projectDescription, preParsedMinimumContribution, manager, preParsedRequestsCount, preParsedApproversCount, balance] = await Promise.all(
                    [project.projectTitle(),
                    project.projectDescription(),
                    project.minimumContribution(),
                    project.manager(),
                    project.requestNum(),
                    project.approversCount(),
                    library.getBalance(address)
                    ])
                const etherBalance = ethers.utils.formatEther(balance)
                const minimumContribution = parseInt(preParsedMinimumContribution)
                const requestsCount = parseInt(preParsedRequestsCount)
                const approversCount = parseInt(preParsedApproversCount)
                setProjectData((prevState) => ({
                    ...prevState , projectTitle, projectDescription, minimumContribution, manager, approversCount, etherBalance, requestsCount 
                }))
            }
        }
        getProject();
    }, [active, isContribute, isCreateRequest, address, account, library])

    const items = [
        {
            header: projectData.manager,
            description:
                'The manager created this campagin and can create requests',
            meta: 'Address of Manager',
            style: { overflowWrap: 'break-word' }
        },
        {
            header: projectData.minimumContribution,
            description:
                'You must contribute at least this amount to be approver.',
            meta: 'Minimum Contribution (wei)',
        },
        {
            header: projectData.requestsCount,
            description:
                'A request tries to withdraw money from the contract. Requests must be approved by approvers',
            meta: 'Number of Requests',
        },
        {
            header: projectData.approversCount,
            description:
                'Number of people who have donated to this project',
            meta: 'Number of Approvers',
        },
        {
            header: projectData.etherBalance,
            description:
                'The balance is how much ether left in this project',
            meta: 'Project Contract Balance (ether)',
        },
    ]

    return (
        <div>
            <Row className="mb-3"><Link to="/" ><Button variant="secondary"> Back</Button></Link></Row>
            <Row>
                <Col>
                    <Card.Group centered items={items} />
                    {projectData.manager && !isCreateRequest && projectData.manager === account ?
                        <>
                            <Button style={buttonStyle} onClick={() => setIsCreateRequest(true)} variant="info">New Request</Button>
                        </>
                        : ""}
                    <Link to={`/${address}/requests`}><Button variant="secondary" style={buttonStyle}>View Requests</Button></Link>

                    {isCreateRequest ? <CreateRequest setIsCreateRequest={setIsCreateRequest} /> : <ContributionForm project={project} minimumContribution={projectData.minimumContribution} setIsContribute={setIsContribute} isContribute={isContribute}/>}
                </Col>
            </Row>

        </div>
    )
}
