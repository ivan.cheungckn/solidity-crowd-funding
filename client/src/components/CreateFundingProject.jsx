import React, { useContext, useState } from 'react'
import { Button, Form, Row, Spinner } from 'react-bootstrap';
import { useForm } from 'react-hook-form'
import { useHistory } from 'react-router';
import { Link } from 'react-router-dom';
import BlockchainContext from '../BlockchainContext.js';

export default function CreateFundingProject(props) {
    const labelStyle = {
        textAlign: `left`
    }
    const history = useHistory();
    const blockchainContext = useContext(BlockchainContext);
    const { factoryContract } = blockchainContext;
    const [loading, setLoading] = useState(false)
    const { register, handleSubmit } = useForm();
    const onSubmit = async (data) => {
        let isMounted = true
        try {
            setLoading(true)
            if (factoryContract) {
                const result = await factoryContract.createProject(data.minimumContribution, data.title, data.description)
                const transactionDetails = await result.wait()
                if (!isMounted) return;
                if (transactionDetails.status) {
                    alert("Success!!~~")
                    setLoading(false)
                    history.push("/")
                    return
                }
            }
        } catch (e) {
            console.error(e)
        }
        setLoading(false)

        return (() => {
            isMounted = false
        })

    };
    return (
        <div>
            <Row className="mb-3"><Link to="/" ><Button variant="secondary"> Back</Button></Link></Row>
            <Form onSubmit={handleSubmit(onSubmit)}>
                <Form.Group style={labelStyle} controlId="formTitle">
                    <Form.Label>Title</Form.Label>
                    <Form.Control type="text" placeholder="Enter Title" {...register("title")} />
                </Form.Group>
                <Form.Group style={labelStyle} controlId="formDescription">
                    <Form.Label>Description</Form.Label>
                    <Form.Control type="text" placeholder="Enter Description" {...register("description")} />
                </Form.Group>
                <Form.Group style={labelStyle} controlId="formMinimumContribution">
                    <Form.Label>Minimum Contribution</Form.Label>
                    <Form.Control type="number" placeholder="Enter Minimum Contribution" {...register("minimumContribution")} />
                </Form.Group>
                {loading ?
                    <Spinner animation="border" variant="primary" /> :
                    <Button variant="primary" type="submit">
                        Create
                </Button>
                }

            </Form>
        </div>
    )
}
