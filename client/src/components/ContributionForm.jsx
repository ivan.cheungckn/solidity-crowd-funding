import { useWeb3React } from '@web3-react/core';
import React, { useState } from 'react'
import { Button, Form, Spinner } from 'react-bootstrap';
import { useForm } from 'react-hook-form';

export default function ContributionForm(props) {
    const context = useWeb3React()
    const { account } = context
    const { register, handleSubmit } = useForm();
    const [loading, setLoading] = useState(false)
    const labelStyle = {
        textAlign: `left`
    }
    const onSubmit = async (data) => {
        let isMounted = true
        try {
            setLoading(true)
            if (data.contribution < props.minimumContribution)
            {
                alert("contribution can't less than minimum contribution")
                setLoading(false)
                return
            }
            if (props.project) {
                const result = await props.project.contribute({
                    from: account,
                    value: data.contribution
                })
                const transactionDetails = await result.wait()
                if (!isMounted) return;
                if (transactionDetails.status)
                {
                    props.setIsContribute(!props.isContribute);
                    alert("Success !!")
                }
            }
        } catch (e) {
            console.error(e)
        }
        setLoading(false)

        return (()=>{
            isMounted = false
        })
    }
    return (
        <div>
            <Form onSubmit={handleSubmit(onSubmit)}>
                <Form.Group style={labelStyle} controlId="formMinimumContribution">
                    <Form.Label>Contribution</Form.Label>
                    <Form.Control type="number" placeholder="Enter Contribution" {...register("contribution")} />
                </Form.Group>
                {loading ?
                    <Spinner animation="border" variant="primary" /> :
                    <Button variant="primary" type="submit">
                        Contribute
                </Button>
                }

            </Form>
        </div>
    )
}

