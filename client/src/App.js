import React, { useEffect, useState } from "react";
import FundingProjectFactoryContract from "./contracts/FundingProjectFactory.json";
import dotenv from 'dotenv'

import "./App.css";
import NavbarTop from "components/NavbarTop";
import { Route, Switch } from "react-router";
import Home from "components/Home";
import FundingProject from "components/FundingProject";
import CreateFundingProject from "components/CreateFundingProject";
import { Alert, Container } from "react-bootstrap";
import BlockchainContext from './BlockchainContext.js';
import Requests from "components/Requests";
import { useEagerConnect } from "hooks";
import { useWeb3React } from "@web3-react/core";
import { getContract } from "utils";
import { NETWORK_ID } from "./constants";

function App() {
  dotenv.config();
  const context = useWeb3React()
  const { library, account, chainId } = context

  const [factoryContract, setFactoryContract] = useState(null);

  useEagerConnect()
  useEffect(() => {
    if (library)
    {
      const contract = getContract(FundingProjectFactoryContract.networks[NETWORK_ID]["address"], FundingProjectFactoryContract.abi, library, account)
      setFactoryContract(contract)
    }
  }, [library, account])

  return (
    <div className="App">
      <BlockchainContext.Provider value={{ factoryContract }}>
        <NavbarTop />
        <Container className="container">
          {chainId && chainId === parseInt(NETWORK_ID) ?
            <Switch>
              <Route path="/" exact>
                <Home />
              </Route>
              <Route path="/fundingProject/:address" exact>
                <FundingProject />
              </Route>
              <Route path="/createFundingProject" exact>
                <CreateFundingProject />
              </Route>
              <Route path="/:address/requests" exact>
                <Requests />
              </Route>
            </Switch>
            : <div><Alert variant="info" ><h2>Please  switch  your  wallet  network  to  rinkeby  network 😊 </h2></Alert></div>}
        </Container>
      </BlockchainContext.Provider>
    </div>
  );
}

export default App;
