pragma solidity >=0.7.0 <0.9.0;

contract FundingProjectFactory {
    address[] public deployedProjects;

    function createProject(uint minimum, string memory projectTitle, string memory projectDescription) public {
        address newProject = address(new FundingProject(minimum, msg.sender, projectTitle, projectDescription));
        deployedProjects.push(newProject);
    }

    function getDeployedProjects() public view returns (address[] memory) {
        return deployedProjects;
    }
}

contract FundingProject {
    struct Request {
        string description;
        uint value;
        address payable recipient;
        bool complete;
        uint approvalCount;
        mapping(address => bool) approvals;
    }

    // Request[] public requests;
    address public manager;
    uint public minimumContribution;
    mapping(address => bool) public approvers;
    uint public approversCount;
    uint public requestNum;
    mapping(uint => Request) public requests;
    string public projectTitle;
    string public projectDescription;

    modifier restricted() {
        require((msg.sender == manager));
        _;
    }

    constructor(uint _minimum, address _creator, string memory _projectTitle, string memory _projectDescription) {
        manager = _creator;
        minimumContribution = _minimum;
        projectTitle = _projectTitle;
        projectDescription = _projectDescription;
    }

    function contribute() public payable {
        require(msg.value > minimumContribution);
        if (approvers[msg.sender] == false)
        {
            approvers[msg.sender] = true;
            approversCount++;
        }
    }

    function createRequest(string memory description, uint value, address payable recipient) public restricted {
        uint projectId = requestNum++;
        Request storage request = requests[projectId];
        request.description = description;
        request.value = value;
        request.recipient = recipient;
        request.complete = false;
        request.approvalCount = 0;
    }

    function approveRequest(uint projectId) public {
        Request storage request = requests[projectId];

        require(approvers[msg.sender]);
        require(!request.approvals[msg.sender]);

        request.approvals[msg.sender] = true;
        request.approvalCount++;
    }

    function finalizeRequest(uint projectId) public restricted {
        Request storage request = requests[projectId];

        require(request.approvalCount >= (approversCount / 2));
        require(!request.complete);

        request.recipient.transfer(request.value);
        request.complete = true;

    }

    function checkApproved(uint projectId) public view returns (bool){
        Request storage request = requests[projectId];
        return request.approvals[msg.sender];
    }
}