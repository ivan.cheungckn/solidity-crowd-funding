const path = require("path");
const HDWalletProvider = require('truffle-hdwallet-provider')
const dotenv = require('dotenv')

dotenv.config()

module.exports = {
  // See <http://truffleframework.com/docs/advanced/configuration>
  // to customize your Truffle configuration!
  contracts_build_directory: path.join(__dirname, "client/src/contracts"),
  networks: {
    develop: {
      port: 8545
    },
    rinkeby: {
      provider: function () {
        return new HDWalletProvider(
          process.env.SEED_PHRASE,
          process.env.INFURA_API
        )
      },
      network_id: 4,
      gas: 4500000,
      gasPrice: 10000000000
    }
},
  compilers: {
  solc: {
    version: "^0.8",    // Fetch exact version from solc-bin (default: truffle's version)
      // docker: true,        // Use "0.5.1" you've installed locally with docker (default: false)
      // settings: {          // See the solidity docs for advice about optimization and evmVersion
      //  optimizer: {
      //    enabled: false,
      //    runs: 200
      //  },
      //  evmVersion: "byzantium"
      // }
    }
}
};
